#!/bin/bash

# Copyright Peter Burke 12/4/2018

# define functions first


function installstuff {
    echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
    echo "Installing a whole bunch of packages..."

    start_time_installstuff="$(date -u +%s)"

    time sudo apt-get -y update # 1 min   #Update the list of packages in the software center                                   
    time sudo apt-get -y upgrade # 3.5 min
    time sudo apt-get -y install screen # 0.5 min
    time sudo apt-get -y install tcptrack # 0 min
    time sudo apt-get -y install python  # 6 sec

    time sudo apt-get -y install python-wxgtk2.8 # 4 min 
    time sudo apt-get -y install python-matplotlib # 
    time sudo apt-get -y install python-opencv # 2 min
    time sudo apt-get -y install python-pip # 3 min
    time sudo apt-get -y install python-numpy  # 0 min
    time sudo apt-get -y install python-dev # 0 min
    time sudo apt-get -y install libxml2-dev # 1 min
    time sudo apt-get -y install libxslt-dev # 0.5 min
    time sudo apt-get -y install python-lxml # 0.75 min
    time sudo apt-get -y install python-setuptools # 0 min

    # Python 3 now                                                                                                                              
    time sudo apt-get -y install python3  # 6 sec                                                                                               
    time sudo apt-get -y install python3-matplotlib #                                                                                           
    time sudo apt-get -y install python3-opencv # 2 min                                                                                         
    time sudo apt-get -y install python3-pip # 3 min                                                                                            
    time sudo apt-get -y install python3-numpy  # 0 min                                                                                         
    time sudo apt-get -y install python3-dev # 0 min                                                                                            
    time sudo apt-get -y install python3-lxml # 0.75 min                                                                                        
    time sudo apt-get -y install python3-setuptools # 0 min                                                                                     
    time sudo apt-get -y install python3-genshi # 0 min                                                                                     
    time sudo apt-get -y install python3-lxml-dbg # 0 min                                                                                     
    time sudo apt-get -y install python-lxml-doc # 0 min                                                                                     
    # Done Python 3    

    time sudo apt-get -y install git # 1 min
    time sudo apt-get -y install dh-autoreconf # 1 min
    time sudo apt-get -y install systemd # 0 min
    time sudo apt-get -y install wget # 0 min
    time sudo apt-get -y install emacs # 4.5 min  (seems you have to run this twice)                                             
    time sudo apt-get -y install emacs # 0 min (seems you have to run this twice)                                             
    time sudo apt-get -y install nload # 0.5 min (network monitor, launch with nload -m)                                         
    time sudo apt-get -y install build-essential # 0 min
    time sudo apt-get -y install autossh # 0.5 min
    time sudo apt-get -y install top # 0.5 min
    time sudo apt-get -y install htop # 0.5 min

    time sudo apt-get -y install htop # 0.5 min


    echo "Done installing a whole bunch of packages..."

    time sudo pip install virtualenv
    time sudo pip3 install virtualenv
    
    echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
    echo "pip installing future, pymavlink, mavproxy..."
    time sudo pip install future # 1 min
    time sudo pip install pymavlink # 5.5 min
    time sudo pip install mavproxy # 4 min
    echo "Done pip installing future, pymavlink, mavproxy..."


    echo "pip3 installing future, pymavlink, mavproxy..."
    time sudo pip3 install future # 1 min                                                                                                       
    time sudo pip3 install pymavlink # 5.5 min                                                                                                  
    time sudo pip3 install mavproxy # 4 min                                                                                                     
    echo "Done pip3 installing future, pymavlink, mavproxy..."


    end_time_installstuff="$(date -u +%s)"
    elapsed_installstuff="$(($end_time_installstuff-$start_time_installstuff))"
    echo "Total of $elapsed_installstuff seconds elapsed for installing packages"
    # 38 mins
    
    
}

# Check for any errors, quit if any
check_errors() {
  if ! [ $? = 0 ]
  then
    echo "An error occured! Aborting...."
    exit 1
  fi
}

function installapachestuff {
    time sudo apt-get install apache2
    #time sudo apt-get install php php-curl php-xml libapache2-mod-php php-mysql php-mbstring php-gettext php-fpm -y
    #time sudo apt-get install mysql-server mysql-client -y
    #time sudo apt-get install phpmyadmin
    time sudo pip install django
    sudo apt-get install libapache2-mod-wsgi-py3
}



function fxyz {
    echo "doing function fxyz"
}




#***********************END OF FUNCTION DEFINITIONS******************************

set -x

echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "This will install mavlink router and set it up for you."
echo "See README in 4guav gitlab repo for documentation."


echo "Starting..."

date


start_time="$(date -u +%s)"


installstuff

installapachestuff

date

end_time="$(date -u +%s)"

elapsed="$(($end_time-$start_time))"
echo "Total of $elapsed seconds elapsed for the entire process"


echo "Installation is complete. You will want to restart your Pi to make this work."
echo "No further action should be required. Closing..."

