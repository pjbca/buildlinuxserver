# BuildLinuxServer

this is my personal setup scripts to install the stuff I typically want on my linux server (e.g. AWS, digital ocean, rasp. pi, desktop linux mint, etc)


to do it:


Get the script to install stuff I want:
```
wget https://gitlab.com/pjbca/buildlinuxserver/raw/master/buildlinuxserver.sh
```

Run script to install:
```
sudo chmod 777 ~/buildlinuxserver.sh; sudo ~/buildlinuxserver.sh 2>&1 | tee buildlog.txt 
```

All in one line:
```
wget https://gitlab.com/pjbca/buildlinuxserver/raw/master/buildlinuxserver.sh; sudo chmod 777 ~/buildlinuxserver.sh; sudo ~/buildlinuxserver.sh 2>&1 | tee buildlog.txt 
```